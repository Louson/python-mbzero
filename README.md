# mbzero: simple python bindings for Musicbrainz

mbzero is a simple library to provide bindings to the musicbrainz ws/2 API.

More documentation is available at
[Read the Docs](https://mbzero.readthedocs.io).

## License

The license is BSD-2 to give the maximum permission for hacking and sharing.

## Requirements

This requires `python3`, `python-request`, `python-requests-oauthlib`

## Manual build/installation

Manual build and install requires `python-build` and `python-installer`

You can build the library using `python -m build` and install with `python -m
installer --dest=<destination>` or `python -m installer --prefix=<prefix>`.

## Contribute

Feel free to fork or contribute on
<https://gitlab.com/Louson-public/personal/python-mbzero>. Issues and PR are
welcome as much as much I can support them.

## Test

Test the library with `pytest -v`

## Authors

mbzero is by [Louis Rannou](https://gitlab.com/louson)

### 2023 Musicbrainzngs authors

These bindings is a fork from the python-musicbrainzez project written by
[Alastair Porter](http://github.com/alastair). Contributions had been made
by:

* [Adrian Sampson](https://github.com/sampsyo)
* [Corey Farwell](https://github.com/frewsxcv)
* [Galen Hazelwood](https://github.com/galenhz)
* [Greg Ward](https://github.com/gward)
* [Ian McEwen](https://github.com/ianmcorvidae)
* [Jérémie Detrey](https://github.com/jdetrey)
* [Johannes Dewender](https://github.com/JonnyJD)
* [Michael Marineau](https://github.com/marineam)
* [Patrick Speiser](https://github.com/doskir)
* [Pavan Chander](https://github.com/navap)
* [Paul Bailey](https://github.com/paulbailey)
* [Rui Gonçalves](https://github.com/ruippeixotog)
* [Ryan Helinski](https://github.com/rlhelinski)
* [Sam Doshi](https://github.com/samdoshi)
* [Shadab Zafar](https://github.com/dufferzafar)
* [Simon Chopin](https://github.com/laarmen)
* [Thomas Vander Stichele](https://github.com/thomasvs)
* [Wieland Hoffmann](https://github.com/mineo)

mbzero |release|
================

`mbzero` implements Python bindings of the `MusicBrainz API`_. With this library
you can retrieve all kinds of music metadata from the `MusicBrainz`_ database.

The library also implements bindings for the `OAuth2`_ authentication.

`mbzero` is released under a simplified BSD style license.

.. _`MusicBrainz`: https://musicbrainz.org
.. _`MusicBrainz API`: https://musicbrainz.org/doc/MusicBrainz_API
.. _`OAuth2`: https://musicbrainz.org/doc/Development/OAuth2

Contents
--------

.. toctree::
    :maxdepth: 2

    installation
    usage
    api

Indices and tables
------------------

* :ref:`genindex`
* :ref:`search`

Installation
~~~~~~~~~~~~

Package manager
---------------

If you want the latest stable version of mbzero, the first place to
check is your systems package manager. Being a relatively new library, you
might not be able to find it packaged by your distribution and need to use one
of the alternate installation methods.

PyPI
----

Mbzero is available on the Python Package Index. This makes installing
it with `pip <http://www.pip-installer.org>`_ as easy as::

    pip install mbzero

Git
---

If you want the latest code or even feel like contributing, the code is
available on `GitLab <https://gitlab.com/Louson-public/personal/python-mbzero>`_.

You can easily clone the code with git::

    git clone https://gitlab.com/Louson-public/personal/python-mbzero

Now you can start hacking on the code through pip::

    pip install .

or install it system-wide::

    python -m build
    python -m install

#!/usr/bin/env python

#  SPDX-FileCopyrightText: 2024 Louis Rannou
#
#  SPDX-License-Identifier: BSD-2

from mbzero import mbzrequest as mbr

user_agent = "mbzero-test"


def lookup_test():
    content = mbr.MbzRequestLookup(user_agent, "artist",
                                   "0383dadf-2a4e-4d10-a46a-e9e041da8eb3"
                                   ).send()
    print(content)


def browse_test():
    content = mbr.MbzRequestBrowse(user_agent, "release",
                                   "artist",
                                   "0383dadf-2a4e-4d10-a46a-e9e041da8eb3",
                                   ).send()
    print(content)


def search_test():
    content = mbr.MbzRequestSearch(
        user_agent, "release",
        "%22we%20will%20rock%20you%22%20AND%20arid:0383dadf-2a4e-4d10-a46a-e9e041da8eb3",
        ).send()
    print(content)

    print("---")

    content = mbr.MbzRequestSearch(
        user_agent, "release-group",
        "releases:0",
        ).send()
    print(content)


if __name__ == '__main__':
    lookup_test()
    browse_test()
    search_test()

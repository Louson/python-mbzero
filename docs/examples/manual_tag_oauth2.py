#!/usr/bin/env python

#  SPDX-FileCopyrightText: 2024 Louis Rannou
#
#  SPDX-License-Identifier: BSD-2

from mbzero import (mbzrequest as mbr, mbzauth)
import json

user_agent = "mbzero-0.1"
request_headers = {"User-Agent": user_agent}
xml_post_headers = {"User-Agent": user_agent,
                    "Content-Type": "application/xml; charset=utf-8"}

client_id = ''
client_secret = ''
scope = ['tag', 'rating']

auth = mbzauth.MbzCredentials()


# For testing purpose
token = ""
refresh = ""
# //

if refresh:
    print("| Open new session")
    auth.oauth2_new(token, refresh,
                    client_id=client_id, client_secret=client_secret,
                    scope=scope)
    if not token:
        auth.oauth2_refresh()
elif not refresh:
    url = auth.oauth2_init(client_id, scope=scope)
    if not url:
        print("| No url")
        exit()
    print("| Please visit the following url:\n  %s" % url)

    code = input("Insert the code\n")

    print("| Get tokens")
    tokens = auth.oauth2_confirm(code, client_secret)
    if not tokens:
        print("| No token received")
        exit()
    token = tokens["access_token"]
    refresh = tokens["refresh_token"]
    print("| access token: %s" % token)
    print("| refresh token: %s" % refresh)


# Ask for user tags
print("| Get tags")
# res = auth._oauth2_get(
#     "/artist/0383dadf-2a4e-4d10-a46a-e9e041da8eb3&inc=user-tags",
#     headers=request_headers)
res = mbr.MbzRequestLookup(user_agent, "artist",
                           "0383dadf-2a4e-4d10-a46a-e9e041da8eb3",
                           includes=["user-tags"]).send(credentials=auth)
print("| ", json.loads(res)["user-tags"])

# Set user tag (xml)
print("| Set tag")
xml = b'''<metadata xmlns="http://musicbrainz.org/ns/mmd-2.0#">
    <artist-list>
        <artist id="0383dadf-2a4e-4d10-a46a-e9e041da8eb3">
            <user-tag-list>
                <user-tag><name>glorious</name></user-tag>
            </user-tag-list>
        </artist>
    </artist-list>
</metadata>'''
# res = auth._oauth2_post("/tag",
#                         data=xml, headers=xml_post_headers,
#                         url="https://musicbrainz.org/ws/2",
#                         payload={"client": "mbzerotest-0.1", "fmt": "json"})
# res.raise_for_status()
# print("| ", res.content)
res = mbr.MbzRequest(user_agent).post("/tag", xml, "xml", credentials=auth)
print("| ", res)

#
print("| Get tags")
# res = auth._oauth2_get(
#     "/artist/0383dadf-2a4e-4d10-a46a-e9e041da8eb3&inc=user-tags",
#     headers=request_headers)
res = mbr.MbzRequestLookup(user_agent, "artist",
                           "0383dadf-2a4e-4d10-a46a-e9e041da8eb3",
                           includes=["user-tags"]).send(credentials=auth)
print("| ", json.loads(res)["user-tags"])

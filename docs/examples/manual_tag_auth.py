#!/usr/bin/env python

#  SPDX-FileCopyrightText: 2024 Louis Rannou
#
#  SPDX-License-Identifier: BSD-2

from mbzero import (mbzrequest as mbr, mbzauth)
import json

user_agent = "mbzero-0.1"
request_headers = {"User-Agent": user_agent}
xml_post_headers = {"User-Agent": user_agent,
                    "Content-Type": "application/xml; charset=utf-8"}

user = ""
password = ""
scope = ['tag', 'rating']

auth = mbzauth.MbzCredentials()
auth.auth_set(user, password)


# Ask for user tags
print("| Get tags")
res = mbr.MbzRequestLookup(user_agent, "artist",
                           "0383dadf-2a4e-4d10-a46a-e9e041da8eb3",
                           includes=["user-tags"]).send(credentials=auth)
print("| ", json.loads(res)["user-tags"])

# Set user tag (xml)
print("| Set tag")
xml = b'''<metadata xmlns="http://musicbrainz.org/ns/mmd-2.0#">
    <artist-list>
        <artist id="0383dadf-2a4e-4d10-a46a-e9e041da8eb3">
            <user-tag-list>
                <user-tag><name>glorious</name></user-tag>
            </user-tag-list>
        </artist>
    </artist-list>
</metadata>'''
res = mbr.MbzRequest(user_agent).post("/tag", xml, "xml", credentials=auth)
print("| ", res)

#
print("| Get tags")
res = mbr.MbzRequestLookup(user_agent, "artist",
                           "0383dadf-2a4e-4d10-a46a-e9e041da8eb3",
                           includes=["user-tags"]).send(credentials=auth)
print("| ", json.loads(res)["user-tags"])

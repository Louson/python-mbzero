#!/usr/bin/env python

#  SPDX-FileCopyrightText: 2024 Louis Rannou
#
#  SPDX-License-Identifier: BSD-2

# Include the project directory to the path:
import os, sys
sys.path.insert(0, os.path.abspath('../..'))

from mbzero import (mbzrequest as mbr, mbzauth)

user_agent = "mbzero-test"
request_headers = {"User-Agent": user_agent}

client_id = 't_qwghP5xFRCDzjAPSmJKS6zm0EgCZgH'
client_secret = 'XL1i10D2u5GjonWZHyw88AnvXNodNABa'
scope = ['profile', 'collection', 'submit_barcode']

auth = mbzauth.MbzCredentials()


# For testing purpose
token = ""
refresh = ""
# //

if refresh:
    print("| Open new session")
    auth.oauth2_new(token, refresh,
                    client_id=client_id, client_secret=client_secret,
                    scope=scope)
elif not refresh:
    url = auth.oauth2_init(client_id, scope=scope)
    if not url:
        print("| No url")
        exit()
    print("| Please visit the following url:\n  %s" % url)

    code = input("Insert the code\n")

    print("| Get tokens")
    tokens = auth.oauth2_confirm(code, client_secret)
    if not tokens:
        print("| No token received")
        exit()
    token = tokens["access_token"]
    refresh = tokens["refresh_token"]
    print("| access token: %s" % token)
    print("| refresh token: %s" % refresh)

# Ask for user informations
print("| Get userinfo")
res = auth._oauth2_get("/userinfo", headers=request_headers)
if res and res.ok:
    print("| ", res.content)
elif res:
    print("| Query failed (%s)" % res)
else:
    print("| Query failed")
##


print("| Revoke token")
auth.oauth2_revoke()
# Ask again for user informations, this should fail
print("| Get userinfo")
res = auth._oauth2_get("/userinfo", headers=request_headers)
if res and res.ok:
    print("| ", res.content)
elif res:
    print("| Query failed (%s)" % res)
else:
    print("| Query failed")
#


print("| Refresh token")
token = auth.oauth2_refresh(refresh)
if not token:
    print("| No token received")
    exit()
print("| access token: %s" % token)

# Ask again for user informations
print("| Get userinfo")
res = auth._oauth2_get("/userinfo", headers=request_headers)
if res and res.ok:
    print("| ", res.content)
elif res:
    print("| Query failed (%s)" % res)
else:
    print("| Query failed")
#


# Ask collection
print("| Get collection")
res = auth._oauth2_get("/collection",
                       payload={"fmt": "json"},
                       headers=request_headers,
                       url="https://musicbrainz.org/ws/2")
if res and res.ok:
    print("| ", res.content)
elif res:
    print("| Query failed (%s)" % res)
else:
    print("| Query failed")
#

# Ask again with MbzRequestLookup
print("| Get collection with lookup")
try:
    mbr.MbzRequestLookup(user_agent, "collection", "").send()
    print("| Lookup should have failed without credentials")
    exit()
except:
    res = mbr.MbzRequestLookup(user_agent, "collection", ""
                               ).send(credentials=auth)
if res:
    print("| ", res)
else:
    print("| Lookup failed")
#

# Ask again with MbzRequestBrowse
print("| Get collection with browse")
try:
    res = mbr.MbzRequestBrowse(user_agent, "collection",
                               "artist",
                               "0383dadf-2a4e-4d10-a46a-e9e041da8eb3",
                               includes=["user-collections"],
                               ).send()
    print("| Browse should have failed without credentials")
    exit()
except:
    res = mbr.MbzRequestBrowse(user_agent, "collection",
                               "artist",
                               "0383dadf-2a4e-4d10-a46a-e9e041da8eb3",
                               includes=["user-collections"],
                               ).send(credentials=auth)
if res:
    print("| ", res)
else:
    print("| Browse failed")
#

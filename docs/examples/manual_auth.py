#!/usr/bin/env python

#  SPDX-FileCopyrightText: 2024 Louis Rannou
#
#  SPDX-License-Identifier: BSD-2

from mbzero import (mbzrequest as mbr, mbzauth)

user_agent = "mbzero-test"
request_headers = {"User-Agent": user_agent}

user = ""
password = ""
scope = ['profile', 'collection', 'submit_barcode']

auth = mbzauth.MbzCredentials()
auth.auth_set(user, password)

# Ask again with MbzRequestLookup
print("| Get collection with lookup")
try:
    mbr.MbzRequestLookup(user_agent, "collection", "").send()
    print("| Lookup should have failed without credentials")
    exit()
except:
    res = mbr.MbzRequestLookup(user_agent, "collection", ""
                               ).send(credentials=auth)
if res:
    print("| ", res)
else:
    print("| Lookup failed")
#

# Ask again with MbzRequestBrowse
print("| Get collection with browse")
try:
    res = mbr.MbzRequestBrowse(user_agent, "collection",
                               "artist",
                               "0383dadf-2a4e-4d10-a46a-e9e041da8eb3",
                               includes=["user-collections"],
                               ).send()
    print("| Browse should have failed without credentials")
    exit()
except:
    res = mbr.MbzRequestBrowse(user_agent, "collection",
                               "artist",
                               "0383dadf-2a4e-4d10-a46a-e9e041da8eb3",
                               includes=["user-collections"],
                               ).send(credentials=auth)
if res:
    print("| ", res)
else:
    print("| Browse failed")
#

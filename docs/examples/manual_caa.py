#!/usr/bin/env python

#  SPDX-FileCopyrightText: 2024 Louis Rannou
#
#  SPDX-License-Identifier: BSD-2

import mbzero

user_agent = "mbzero-test"

def lookup_test():
    content = mbzero.CaaRequest(user_agent, "release",
                                "76df3287-6cda-33eb-8e9a-044b5e15ffdd"
                                ).send()
    print(content)


def lookup_head_test():
    content = mbzero.CaaRequest(user_agent, "release",
                                "76df3287-6cda-33eb-8e9a-044b5e15ffdd"
                                ).send(head=True)
    print(content)


if __name__ == '__main__':
    lookup_test()
    lookup_head_test()

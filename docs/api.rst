References
==========

.. toctree::
   :maxdepth: 2
   :caption: Contents:


.. _api_requests:

Requests
--------

.. automodule:: mbzero.mbzrequest
    :members:

.. _api_authentication:

Authentication
--------------

.. automodule:: mbzero.mbzauth
    :members:

.. _api_caa:

Cover Art Archive
-----------------

.. automodule:: mbzero.caarequest
    :members:

.. _api_errors:

Errors
------

.. automodule:: mbzero.mbzerror
    :members:

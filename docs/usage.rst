Usage
=====

.. toctree::
   :maxdepth: 2
   :caption: Contents:

The library almost matches the `MusicBrainz API`_. Most of terms suche as
lookup, browse, request will be related to MusicBrainz definitions.

As MusicBrainz requires the user-agent to be defined, it will be mandatory in
this library.

If you also want to submit data,
then you must authenticate as a MusicBrainz user.

This part of the documentation will give you usage examples.
For an overview of available functions you can have a look at
the :doc:`api`.

Identification
--------------

To access the MusicBrainz webservice through this library, you `need to identify
your application
<https://musicbrainz.org/doc/MusicBrainz_API#Application_rate_limiting_and_identification>`_
by setting the useragent header made in HTTP requests to one that is unique to
your application. To do so, you will have to provide it in the format
`recommended by MusicBrainz
<https://musicbrainz.org/doc/MusicBrainz_API/Rate_Limiting#Provide_meaningful_User-Agent_strings>`_.

Requests
--------

To make a request, use the module ``mbzrequest``::

    from mbzero import mbzrequest
    USER_AGENT = "mbz-test/0.0 <someone@example.com>"

    content = mbr.MbzRequestLookup(user_agent, "artist",
                                   "0383dadf-2a4e-4d10-a46a-e9e041da8eb3"
                                   ).send()
    content = mbr.MbzRequestBrowse(user_agent, "release",
                                   "artist",
                                   "0383dadf-2a4e-4d10-a46a-e9e041da8eb3",
                                   ).send()
    content = mbr.MbzRequestSearch(
        user_agent, "release-group",
        "releases:0",
        ).send()

Four classes are available, one general and three that match the three types of
request Search, Lookup and Browse (as describe in `MusicBrainz API`_). Each
require at least a user-agent and each have a method ``send``. Look at
:ref:`api_requests` for details.

Authentication
--------------

There are two authentications method available: digest and `OAuth2`_. Both
require the module ``mbzauth`` and use the class ``MbzCredentials``.

For an insecure authentication, one may use its secret password::

    from mbzero import mbzauth

    # insecure authentication
    auth = mbzauth.MbzCredentials()
    auth.auth_set(user, password)

For an secure authentication, you must follow the OAuth2 process.

1. Register an application on `MusicBrainz`_. It can be done on the `user's
   application settings <https://musicbrainz.org/account/applications>`_. Get
   the client ID and its secret.
2. Use ``oauth2_init`` to get the authorization URL. Specify the server if not
   MusicBrainz. Specify the scope for what you needs.
3. Visit and login to the given URL. Get the authorization code.
4. Use ``oauth2_confirm`` to confirm the authentication.

Example::

    # secure authentication
    client_id = '<client_id>'
    client_secret = '<client_secret>'
    scope = ['profile', 'collection', 'submit_barcode']

    auth = mbzauth.MbzCredentials()
    url = auth.oauth2_init(client_id, scope=scope)
    print("Please visit the following url:\n  %s" % url)

    code = input("Insert the code\n")
    tokens = auth.oauth2_confirm(code, client_secret)

When the access token is known, use oauth2_new to skip this initialization::

    auth.oauth2_new(access_token, refresh_token=refresh_token,
                   client_id=client_id, client_secret=client_secret,
		   scope=scope)

Use ``oauth2_refresh`` to refresh the access token using the refresh token::

    auth.oauth2_refresh()

Authenticated requests
~~~~~~~~~~~~~~~~~~~~~~

For both method, the credentials must be provided in the send request::

    res = mbr.MbzRequestLookup(user_agent, "collection", ""
                              ).send(credentials=auth)

Cover Art Archive
~~~~~~~~~~~~~~~~~

The Cover Art Archive binding follows the `CAA API`_. One may use the
``caarequest`` module::

    from mbzero import caarequest

    content = caa.CaaRequest(user_agent, "artist",
                             "0383dadf-2a4e-4d10-a46a-e9e041da8eb3"
                             ).send()

    content = caa.CaaRequest(user_agent, "artist",
                             "0383dadf-2a4e-4d10-a46a-e9e041da8eb3"
                             ).send(head=True)

.. _`CAA API`: https://musicbrainz.org/doc/Cover_Art_Archive/API#OPTIONS_support

Exceptions
----------

Handling of exceptions requires the module ``mbzerror``::

   from mbzero import mbzerror

.. _`MusicBrainz`: https://musicbrainz.org
.. _`MusicBrainz API`: https://musicbrainz.org/doc/MusicBrainz_API
.. _`OAuth2`: https://musicbrainz.org/doc/Development/OAuth2
